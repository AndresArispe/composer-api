package com.milanka.composer.models.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OrderAddress {

    private String addressLine1;
    private String addressLine2;
    private String contactName;
    private String contactPhoneNumber;
    private String state;
    private String city;
    private String zipCode;
    private String countryCode;

}
