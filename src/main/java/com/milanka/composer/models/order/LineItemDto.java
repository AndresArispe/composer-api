package com.milanka.composer.models.order;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class LineItemDto {


    private UUID productId;
    private int qty;

}
