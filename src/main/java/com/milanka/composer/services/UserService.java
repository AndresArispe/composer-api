package com.milanka.composer.services;

import com.milanka.composer.models.authentication.AuthenticationRequest;
import com.milanka.composer.models.user.NewPassword;
import com.milanka.composer.models.user.User;
import com.milanka.composer.models.user.UserInDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Value("${user.api.uri}")
    private String endpoint;

    private WebClient.Builder webClientBuilder;

    public UserService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public String comprobeUser(UUID userId) {
        try {
            User user = webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                    .build()
                    .get()
                    .uri("/v1/users/"+userId)
                    .retrieve()
                    .bodyToMono(User.class).block();
        } catch (Exception e) {
            return ""+e;
        }
        return "Existing User";
    }

    public List<User> getUsers() {
        List<User> allUsers= new ArrayList<>();
        allUsers = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .get()
                .uri("/v1/users")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<User>>() {})
                .block();
        return allUsers;
    }

    public User getUserById(UUID id) {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .get()
                .uri("/v1/users/" + id)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    public String getAlive() {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .get()
                .uri("/v1/users/healthcheck")
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public User createUser(UserInDto user) {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .post()
                .uri("/v1/users")
                .body(BodyInserters.fromPublisher(Mono.just(user), UserInDto.class))
                .retrieve()
                .bodyToMono(User.class).block();
    }

    public int deleteUser(UUID userId) {
        try {
            webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                    .build()
                    .delete()
                    .uri("/v1/users/" + userId)
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
            return 204;
        }catch (WebClientResponseException e) {
            return e.getRawStatusCode();
        }
    }

    public User updateUser(UserInDto userInDto, UUID userId) {
        User user1 = new User();
        user1 = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .patch()
                .uri("/v1/users/" + userId)
                .body(BodyInserters.fromPublisher(Mono.just(userInDto), UserInDto.class))
                .retrieve()
                .bodyToMono(User.class).block();
        return user1;
    }

    public String addingAuthentication(AuthenticationRequest authenticationRequest) {
        String userResponse = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .post()
                .uri("/v1/users/authenticate")
                .body(BodyInserters.fromPublisher(Mono.just(authenticationRequest), AuthenticationRequest.class))
                .retrieve()
                .bodyToMono(String.class).block();
        return userResponse;
    }

    public String updatePassword(NewPassword newPassword, UUID userId) {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("user","users.123"))
                .build()
                .patch()
                .uri("/v1/users/" + userId + "/change-password")
                .body(BodyInserters.fromPublisher(Mono.just(newPassword), NewPassword.class))
                .retrieve()
                .bodyToMono(String.class).block();
    }
}
