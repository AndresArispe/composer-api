package com.milanka.composer.models.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDto {

    private String addressLine1;
    private String addressLine2;
    private String state;
    private String city;
    private String zipCode;
    private String countryCode;

}
