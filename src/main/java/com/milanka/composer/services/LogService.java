package com.milanka.composer.services;

import com.milanka.composer.models.log.LogStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Value("${log.api.uri}")
    private String endpoint;

    private WebClient.Builder webClientBuilder;

    public LogService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public List<LogStatus> getLogs(String level, String service, String message, String fromDate, String toDate) {
        List<LogStatus> logStatuses = new ArrayList<>();
        logStatuses = webClientBuilder
                .baseUrl(endpoint)
                .build()
                .get()
                .uri("/v1/logs?level=" + level + "&service=" + service + "&message=" + message + "&fromDate=" + fromDate + "&toDate=" + toDate)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<LogStatus>>() {})
                .block();
        return logStatuses;
    }

    public List<LogStatus> getLogsWithoutDate(String level, String service, String message) {
        List<LogStatus> logStatuses = new ArrayList<>();
        logStatuses = webClientBuilder
                .baseUrl(endpoint)
                .build()
                .get()
                .uri("/v1/logs?level=" + level + "&service=" + service + "&message=" + message)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<LogStatus>>() {})
                .block();
        return logStatuses;
    }

    public List<LogStatus> getLogsWithFromDate(String level, String service, String message, String fromDate) {
        List<LogStatus> logStatuses = new ArrayList<>();
        logStatuses = webClientBuilder
                .baseUrl(endpoint)
                .build()
                .get()
                .uri("/v1/logs?level=" + level + "&service=" + service + "&message=" + message + "&fromDate=" + fromDate)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<LogStatus>>() {})
                .block();
        return logStatuses;
    }


}
