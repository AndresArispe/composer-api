package com.milanka.composer.controllers;

import com.milanka.composer.jwt.JwtUtils;
import com.milanka.composer.models.authentication.AuthenticationRequest;
import com.milanka.composer.models.authentication.AuthenticationResponse;
import com.milanka.composer.models.company.Company;
import com.milanka.composer.models.company.in.CompanyDto;
import com.milanka.composer.models.order.Order;
import com.milanka.composer.models.product.Product;
import com.milanka.composer.models.user.NewPassword;
import com.milanka.composer.models.user.User;
import com.milanka.composer.models.user.UserInDto;
import com.milanka.composer.services.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class ComposerController {

    private JwtUtils jwtUtils;
    private MyUserDetailsService userDetailsService;
    private AuthenticationManager authenticationManager;
    private UserService userService;
    private OrderService orderService;
    private ProductService productService;
    private CompanyService companyService;
    private LogService logService;

    public ComposerController(LogService logService, JwtUtils jwtUtils, MyUserDetailsService userDetailsService, AuthenticationManager authenticationManager, UserService userService, OrderService orderService, ProductService productService, CompanyService companyService) {
        this.jwtUtils = jwtUtils;
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.orderService = orderService;
        this.productService = productService;
        this.companyService = companyService;
        this.logService = logService;
    }

    //Composer

    @GetMapping("/v1/user/{userId}/orders")
    public List<Order> getOrdersFromUsers(@PathVariable UUID userId) {
        String message = userService.comprobeUser(userId);
        if (message != "Existing User") {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user doesn't exist"
            );
        }
        else {
            List<Order> orders = new ArrayList<>();
            orders = orderService.getOrdersByUser(userId);
            return orders;
        }
    }

    @GetMapping("/v1/user/{userId}/products")
    public List<Product> getProductsFromUser(@PathVariable UUID userId) {
        String message = userService.comprobeUser(userId);
        if (message != "Existing User") {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user doesn't exist"
            );
        }
        else {
            List<Product> allProducts = new ArrayList<>();
            List<UUID> productsId = new ArrayList<>();
            allProducts = productService.getProducts();
            productsId = productService.getProductsIdByUser(userId);
            List<UUID> finalProductsId = productsId;
            List<Product> userProducts =
                    allProducts.stream()
                            .filter(product -> finalProductsId.contains(product.getId()))
                            .collect(Collectors.toList());
            return userProducts;
        }
    }

    @GetMapping("/v1/company/{companyId}/products")
    public List<Product> getProductsByCompany(@PathVariable UUID companyId) {
        String message = companyService.comprobeCompany(companyId);
        if (message != "Existing Company") {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "company doesn't exist"
            );
        }
        else {
            List<Product> companyProducts = new ArrayList<>();
            companyProducts = productService.getProductsByCompany(companyId);
            return companyProducts;
        }
    }

    //Company

    @GetMapping("/v1/companies/healthcheck")
    public String getCompanyAlive() {
        return companyService.getAlive();
    }

    @GetMapping("/v1/companies")
    public List<Company> getCompanies() {
        return companyService.getCompanies();
    }

    @GetMapping("/v1/companies/{companyId}")
    public Company getCompany(@PathVariable UUID companyId) {
        if (companyService.getCompany(companyId) == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }
        else {
            return companyService.getCompany(companyId);
        }
    }

    @PostMapping("/v1/companies")
    public ResponseEntity<Object> postCompany(@RequestBody CompanyDto companyDto) {
        try {
            return new ResponseEntity<>(companyService.createCompany(companyDto), HttpStatus.CREATED);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @DeleteMapping("/v1/companies/{companyId}")
    public ResponseEntity<Object> deleteCompany(@PathVariable UUID companyId) {
        try {
            return new ResponseEntity<>(companyId.toString(), HttpStatus.resolve(companyService.deleteCompany(companyId)));
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(companyService.deleteCompany(companyId)));
        }
    }

    @PatchMapping("/v1/companies/{companyId}")
    public ResponseEntity<Object> patchCompany(@PathVariable UUID companyId, @RequestBody CompanyDto companyDto) {
        try {
            return new ResponseEntity<>(companyService.updateCompany(companyDto, companyId), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    //Product

    @GetMapping("/v1/products")
    public List<Product> getAllProducts() {
        return productService.getProducts();
    }

    @GetMapping("/v1/products/{id}")
    public ResponseEntity<Object> getProduct(@PathVariable UUID id) {
        try {
            return new ResponseEntity<>(productService.getProductById(id), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @GetMapping("/v1/products/healthcheck")
    public String getProductAlive() {
        return productService.getAlive();
    }

    @PostMapping(value = "/v1/products")
    public ResponseEntity<Object> postProduct(@RequestBody Product product) {
        try {
            return new ResponseEntity<>(productService.createProduct(product), HttpStatus.CREATED);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @DeleteMapping("/v1/products/{productId}")
    public ResponseEntity<Object> deleteProduct(@PathVariable UUID productId) {
        try {
            return new ResponseEntity<>(productId.toString(), HttpStatus.resolve(productService.deleteProduct(productId)));
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(productService.deleteProduct(productId)));
        }
    }

    @PatchMapping("/v1/products/{productId}")
    public ResponseEntity<Object> patchProduct(@PathVariable UUID productId, @RequestBody Product product) {
        try {
            return new ResponseEntity<>(productService.updateProduct(product, productId), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    //Order

    @GetMapping("/v1/orders")
    public List<Order> getAllOrders() {
        return orderService.getOrders();
    }

    @GetMapping("/v1/orders/{id}")
    public ResponseEntity<Object> getOrder(@PathVariable UUID id) {
        try {
            return new ResponseEntity<>(orderService.getOrderById(id), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @GetMapping("/v1/orders/healthcheck")
    public String getOrderAlive() {
        return orderService.getAlive();
    }

    @PostMapping(value = "/v1/orders")
    public ResponseEntity<Object> postOrder(@RequestBody Order order) {
        try {
            return new ResponseEntity<>(orderService.createOrder(order), HttpStatus.CREATED);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @DeleteMapping("/v1/orders/{orderId}")
    public ResponseEntity<Object> deleteOrder(@PathVariable UUID orderId) {
        try {
            return new ResponseEntity<>(orderId.toString(), HttpStatus.resolve(orderService.deleteOrder(orderId)));
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(orderService.deleteOrder(orderId)));
        }
    }

    @PatchMapping("/v1/orders/{orderId}")
    public ResponseEntity<Object> patchOrder(@PathVariable UUID orderId, @RequestBody Order order) {
        try {
            return new ResponseEntity<>(orderService.updateOrder(order, orderId), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    //User

    @GetMapping("/v1/users")
    public List<User> getAllUsers() {
        return userService.getUsers();
    }

    @GetMapping("/v1/users/{id}")
    public ResponseEntity<Object> getUser(@PathVariable UUID id) {
        try {
            return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @GetMapping("/v1/users/healthcheck")
    public String getUserAlive() {
        return userService.getAlive();
    }

    @PostMapping(value = "/v1/users")
    public ResponseEntity<Object> postUser(@RequestBody UserInDto userInDto) {
        try {
            return new ResponseEntity<>(userService.createUser(userInDto), HttpStatus.CREATED);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @DeleteMapping("/v1/users/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable UUID userId) {
        try {
            return new ResponseEntity<>(userId.toString(), HttpStatus.resolve(userService.deleteUser(userId)));
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(userService.deleteUser(userId)));
        }
    }

    @PatchMapping("/v1/users/{userId}")
    public ResponseEntity<Object> patchUser(@PathVariable UUID userId, @RequestBody UserInDto userInDto) {
        try {
            return new ResponseEntity<>(userService.updateUser(userInDto, userId), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    @PatchMapping("/v1/users/{userId}/change-password")
    public ResponseEntity<Object> patchUser(@PathVariable UUID userId, @RequestBody NewPassword newPassword) {
        try {
            return new ResponseEntity<>(userService.updatePassword(newPassword, userId), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    //jwt

    @PostMapping("/v1/users/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            return new ResponseEntity<>(userService.addingAuthentication(authenticationRequest), HttpStatus.OK);
        }
        catch (WebClientResponseException e) {
            return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
        }
    }

    //Rabbit MQ

    @GetMapping(value = "/v1/logs")
    public ResponseEntity<Object> getLogsByDates(
            @RequestParam(value = "level", required = false, defaultValue = "") String level,
            @RequestParam(value = "service", required = false, defaultValue = "") String service,
            @RequestParam(value = "message", required = false, defaultValue = "") String message,
            @RequestParam(value = "fromDate", required = false) String fromDate,
            @RequestParam(value = "toDate", required = false) String toDate) {
        if (fromDate == null && toDate == null) {
            try {
                return new ResponseEntity<>(logService.getLogsWithoutDate(level, service, message), HttpStatus.OK);
            } catch (WebClientResponseException e) {
                return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
            }
        }
        else {
            if (fromDate != null && toDate == null) {
                try {
                    return new ResponseEntity<>(logService.getLogsWithFromDate(level, service, message, fromDate), HttpStatus.OK);
                } catch (WebClientResponseException e) {
                    return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
                }
            }
            else {
                try {
                    return new ResponseEntity<>(logService.getLogs(level, service, message, fromDate, toDate), HttpStatus.OK);
                } catch (WebClientResponseException e) {
                    return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.resolve(e.getRawStatusCode()));
                }
            }
        }
    }

    //Composer Healthcheck

    @GetMapping("/v1/composer/healthcheck")
    public String healthCheck() {
        return "I'm Alive";
    }

}
