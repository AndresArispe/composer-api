package com.milanka.composer.services;



import com.milanka.composer.models.order.Order;
import com.milanka.composer.models.product.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ProductService {

    @Value("${product.api.uri}")
    private String endpoint;

    private WebClient.Builder webClientBuilder;
    private OrderService orderService;

    public ProductService(WebClient.Builder webClientBuilder, OrderService orderService) {
        this.webClientBuilder = webClientBuilder;
        this.orderService = orderService;
    }

    public List<Product> getProducts() {
        List<Product> allProducts= new ArrayList<>();
        allProducts = webClientBuilder
                        .baseUrl(endpoint)
                        .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
                        .build()
                        .get()
                        .uri("/v1/products")
                        .retrieve()
                        .bodyToMono(new ParameterizedTypeReference<List<Product>>() {})
                        .block();
        return allProducts;
    }

    public List<UUID> getProductsIdByUser(UUID userId) {
        List<UUID> productsId = new ArrayList<>();
        List<Order> ordersFromUser = new ArrayList<>();
        ordersFromUser = orderService.getOrdersByUser(userId);
        ordersFromUser.stream().forEach(order -> order.getLineItems().stream().forEach(lineItemDto -> productsId.add(lineItemDto.getProductId())));
        return productsId;
    }

    public List<Product> getProductsByCompany(UUID companyId) {
        List<Product> products = new ArrayList<>();
        products = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
                .build()
                .get()
                .uri("/v1/products")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Product>>() {})
                .block();
        products.removeIf(product -> !product.getCompanyId().equals(companyId));
        return products;
    }

    public Product getProductById(UUID id) {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
                .build()
                .get()
                .uri("/v1/products/" + id)
                .retrieve()
                .bodyToMono(Product.class)
                .block();
    }

    public String getAlive() {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
                .build()
                .get()
                .uri("/v1/products/healthcheck")
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public Product createProduct(Product product) {
        return webClientBuilder
            .baseUrl(endpoint)
            .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
            .build()
            .post()
            .uri("/v1/products")
            .body(BodyInserters.fromPublisher(Mono.just(product), Product.class))
            .retrieve()
            .bodyToMono(Product.class).block();
    }

    public int deleteProduct(UUID productId) {
        try {
            webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
                    .build()
                    .delete()
                    .uri("/v1/products/" + productId)
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
            return 204;
        }catch (WebClientResponseException e) {
            return e.getRawStatusCode();
        }
    }

    public Product updateProduct(Product product, UUID productId) {
        Product product1 = new Product();
        product1 = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("product","products.123"))
                .build()
                .patch()
                .uri("/v1/products/" + productId)
                .body(BodyInserters.fromPublisher(Mono.just(product), Product.class))
                .retrieve()
                .bodyToMono(Product.class).block();
        return product1;
    }
}
