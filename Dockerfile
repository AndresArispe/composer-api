FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ENV APP_PORT=9191
ENV PRODUCT_API_BASE_URL=http://springboot-product:8090
ENV ORDER_API_BASE_URL=http://springboot-order:8091
ENV COMPANY_API_BASE_URL=http://springboot-company:8092
ENV USER_API_BASE_URL=http://springboot-user:8093
ENV COMPOSER_SERVICE_V1_URL=http://springboot-composer:8094
ENV LOG_API_BASE_URL=http://springboot-composer:8096
ARG JAR_FILE=target/*.jar
ARG RSA=*.pem
ADD ${JAR_FILE} api.jar
ADD ${RSA} /
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","api.jar"]