package com.milanka.composer.models.company;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class Company {

    private UUID id;

    private String name;
    private AddressDto address;
}
