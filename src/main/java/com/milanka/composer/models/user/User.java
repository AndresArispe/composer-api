package com.milanka.composer.models.user;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class User {

    private UUID id;

    private String firstName;
    private String lastName;
    private String email;

}
