package com.milanka.composer.services;

import com.milanka.composer.models.order.Order;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService {

    @Value("${order.api.uri}")
    private String endpoint;

    private WebClient.Builder webClientBuilder;

    public OrderService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public List<Order> getOrdersByUser(UUID userId) {
        List<Order> orders = new ArrayList<>();
        orders = webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                    .build()
                    .get()
                    .uri("/v1/orders")
                    .retrieve()
                    .bodyToMono(new ParameterizedTypeReference<List<Order>>() {})
                    .block();
        orders.removeIf(order -> !order.getUserId().equals(userId));
        return orders;
    }

    public List<Order> getOrders() {
        List<Order> allOrders= new ArrayList<>();
        allOrders = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                .build()
                .get()
                .uri("/v1/orders")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Order>>() {})
                .block();
        return allOrders;
    }

    public Order getOrderById(UUID id) {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                .build()
                .get()
                .uri("/v1/orders/" + id)
                .retrieve()
                .bodyToMono(Order.class)
                .block();
    }

    public String getAlive() {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                .build()
                .get()
                .uri("/v1/orders/healthcheck")
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public Order createOrder(Order order) {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                .build()
                .post()
                .uri("/v1/orders")
                .body(BodyInserters.fromPublisher(Mono.just(order), Order.class))
                .retrieve()
                .bodyToMono(Order.class).block();
    }

    public int deleteOrder(UUID orderId) {
        try {
            webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                    .build()
                    .delete()
                    .uri("/v1/orders/" + orderId)
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
            return 204;
        }catch (WebClientResponseException e) {
            return e.getRawStatusCode();
        }
    }

    public Order updateOrder(Order order, UUID orderId) {
        Order order1 = new Order();
        order1 = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("order","orders.123"))
                .build()
                .patch()
                .uri("/v1/orders/" + orderId)
                .body(BodyInserters.fromPublisher(Mono.just(order), Order.class))
                .retrieve()
                .bodyToMono(Order.class).block();
        return order1;
    }
}
