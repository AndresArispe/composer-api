package com.milanka.composer.services;

import com.milanka.composer.models.company.Company;
import com.milanka.composer.models.company.in.CompanyDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CompanyService {

    @Value("${company.api.uri}")
    private String endpoint;

    private WebClient.Builder webClientBuilder;

    public CompanyService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }


    public String comprobeCompany(UUID companyId) {
        try {
            Company company = webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                    .build()
                    .get()
                    .uri("/v1/companies/"+companyId)
                    .retrieve()
                    .bodyToMono(Company.class).block();
        } catch (Exception e) {
            return ""+e;
        }
        return "Existing Company";
    }

    public List<Company> getCompanies() {
        List<Company> companies = new ArrayList<>();
        companies = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                .build()
                .get()
                .uri("/v1/companies")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Company>>() {})
                .block();
        return companies;
    }

    public Company getCompany(UUID companyId) {
        Company company = new Company();
        try {
            company = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                .build()
                .get()
                .uri("/v1/companies/"+companyId)
                .retrieve()
                .bodyToMono(Company.class)
                .block();
            return company;
        }catch (Exception e) {
            return null;
        }


    }

    public Company createCompany(CompanyDto companyDto) {
         return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                .build()
                .post()
                .uri("/v1/companies")
                .body(BodyInserters.fromPublisher(Mono.just(companyDto), CompanyDto.class))
                .retrieve()
                .bodyToMono(Company.class).block();

    }

    public String getAlive() {
        return webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                .build()
                .get()
                .uri("/v1/companies/healthcheck")
                .retrieve()
                .bodyToMono(String.class).block();
    }

    public int deleteCompany(UUID companyId) {
        try {
            webClientBuilder
                    .baseUrl(endpoint)
                    .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                    .build()
                    .delete()
                    .uri("/v1/companies/"+companyId)
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
            return 204;
        }catch (WebClientResponseException e) {
            return e.getRawStatusCode();
        }
    }

    public Company updateCompany(CompanyDto companyDto, UUID companyId) {
        Company company = new Company();
        company = webClientBuilder
                .baseUrl(endpoint)
                .filter(ExchangeFilterFunctions.basicAuthentication("company","companies.123"))
                .build()
                .patch()
                .uri("/v1/companies/" + companyId)
                .body(BodyInserters.fromPublisher(Mono.just(companyDto), CompanyDto.class))
                .retrieve()
                .bodyToMono(Company.class).block();
        return company;
    }
}
