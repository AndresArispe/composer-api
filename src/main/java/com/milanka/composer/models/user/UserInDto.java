package com.milanka.composer.models.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserInDto {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
