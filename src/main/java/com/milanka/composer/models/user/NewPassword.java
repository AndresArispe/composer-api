package com.milanka.composer.models.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewPassword {

    String actualPassword;
    String newPassword;
}
