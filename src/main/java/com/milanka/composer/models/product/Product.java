package com.milanka.composer.models.product;

import lombok.Getter;
import lombok.Setter;
import java.util.UUID;

@Getter
@Setter
public class Product {

    private UUID id;
    private String name;
    private String [] description;
    private UUID companyId;
    private transient Boolean blocked;
    private String [] categories;

}
