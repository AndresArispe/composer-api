package com.milanka.composer.models.order;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class Order {

    private UUID id;

    private UUID userId;
    private String emailAddress;
    private OrderAddress address;
    private List<LineItemDto> lineItems = new ArrayList<>();
}
