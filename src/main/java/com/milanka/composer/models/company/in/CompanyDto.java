package com.milanka.composer.models.company.in;

import com.milanka.composer.models.company.AddressDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyDto {

    private String name;
    private AddressDto address;
}
