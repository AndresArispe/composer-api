package com.milanka.composer.models.log;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class LogStatus {

    private UUID id;

    private String level;
    private Date dateAndTime;
    private String serviceName;
    private String message;

}
